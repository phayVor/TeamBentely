# Rectify

Rectify is a project under the learnFactory Internship development loop. The app seeks to take an input from the user and return the word equivalent of the parsed number.

## Project Team

> Team BENTELY-CONTINENTAL

## Team Supervisor

* Mr Obidi Isaac

## Project Credits

* Chidera Paul Ugwuanyi - dexiouz@gmail.com
* Chinenye Juliet Etumnu - julivio989@gmail.com
* Chuba
* George Phavor - phavorsparks@gmail.com
